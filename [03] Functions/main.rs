// Josef Frank :: 2015
// Usage of functions

fn main() {

  // Immutable variable
  let y: f32 = 4.9;
  
  // Gather variable from function
  let z: f32 = square_to(5.9, y);
  
  println!("z = {}", z);
}


fn square_to(x: f32, y: f32) -> f32 {
  
  // Can use 'return 1;' as well, poor form at end of function
  
  // No semicolon means this is the return value
  x.powf(y) as f32
}


fn diverges() -> ! {
  
  panic!("This function never returns!");
}