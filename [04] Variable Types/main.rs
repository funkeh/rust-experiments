// Josef Frank :: 2015
// Usage of variable types

fn main() {

  // Booleans
  let xBool = true;
  let yBool: bool = false;
}