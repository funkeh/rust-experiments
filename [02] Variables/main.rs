// Josef Frank :: 2015
// Usage of variables

fn main() {

  // Single variable assignment
  let x = 5;
  
  // Multiple variable assignment
  let (y, z) = (1, 2);
  
  // Assign specific type (32-bit integer)
  let a: i32 = 74;
  
  // Single mutable variable declaration
  let mut b = 5;
  println!("Original value of b was {}", b);
  
  // Mutable variable assignment
  b = a * x * y * z;
  println!("a * x * y * z = {}", b);
}